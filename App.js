import React ,{useState}from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Display from './components/Display'
import Buttons from './components/Buttons'

export default function App() {
  const [expression,setExpression] = useState('')
  return (
    <View style={styles.container}>
      <Display text={expression}/>
      <Buttons setExpr={setExpression}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
});
