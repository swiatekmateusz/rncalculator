import React from 'react';
import { StyleSheet, Text, View , TouchableOpacity } from 'react-native';
import reducer from './reducer'

const Button = ({content,right,set}) => {
  const styles = StyleSheet.create({
    container: {
      height: right ? "20%": "25%",
      width: right ? "100%":"33.33%",
      justifyContent: 'center',
      alignItems: 'center',
    },
    text:{
      fontSize: 48,
      color: "white",
    }
  })
  return (
    <TouchableOpacity  style={styles.container} onPress={()=>reducer(content,set)}>
      <Text style={styles.text}>{content}</Text>
    </TouchableOpacity>
  );
}




export default Button;