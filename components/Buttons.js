import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Button from './Button'

const Buttons = ({setExpr}) => {
  const leftSite = ['1','2','3','4','5','6','7','8','9','.','0',"="]
  const rightSite = ["C","/","*","-","+"]
  return ( 
    <View style={styles.container}>
      <View style={styles.left}>
        {leftSite.map(item=><Button content={item} key={item} set={setExpr}/>)}
      </View>
      <View style={styles.right}>
        {rightSite.map(item=><Button content={item} key={item} set={setExpr} right={true}/>)}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    flex: 7,
    backgroundColor: '#555',
  },
  right:{
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: "row"
  },
  left:{
    backgroundColor: "#444",
    flex: 3,
    flexWrap: 'wrap',
    flexDirection: "row"
  }
});



export default Buttons;