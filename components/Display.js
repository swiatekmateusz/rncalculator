import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Display = ({text}) => {
  return ( 
    <View style={styles.container}>
      <Text style={styles.text}>{text}</Text>
    </View>
   );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#ddd',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: "10%",
    
  },
  text:{
    fontSize: 48,
  }
});



export default Display;